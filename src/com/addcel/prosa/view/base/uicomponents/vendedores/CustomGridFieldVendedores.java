package com.addcel.prosa.view.base.uicomponents.vendedores;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.container.VerticalFieldManager;

import com.addcel.prosa.dto.Vendedor;
import com.addcel.prosa.view.base.UtilColor;

public class CustomGridFieldVendedores extends VerticalFieldManager {

	private Vendedor vendedor;
	
	public CustomGridFieldVendedores(Vendedor vendedor) {

		super();

		this.vendedor = vendedor;
		
		VendedorRichTextField option1 = new VendedorRichTextField(vendedor, VendedorRichTextField.NOMBRE);
		VendedorRichTextField option2 = new VendedorRichTextField(vendedor, VendedorRichTextField.LOGIN);
		VendedorRichTextField option3 = new VendedorRichTextField(vendedor, VendedorRichTextField.ROLE);
		VendedorRichTextField option4 = new VendedorRichTextField(vendedor, VendedorRichTextField.STATUS);
		
		add(option1);
		add(option2);
		add(option3);
		add(option4);
		add(new NullField());
	}

	protected void paint(Graphics graphics) {

		graphics.clear();

		if (isFocus()) {
			graphics.setColor(UtilColor.LIST_BACKGROUND_SELECTED);
			graphics.setGlobalAlpha(70);
			graphics.fillRoundRect(0, 0, getPreferredWidth(),
					getPreferredHeight(), 1, 1);
			graphics.setGlobalAlpha(255);
		} else {
			graphics.setColor(UtilColor.LIST_BACKGROUND_UNSELECTED);
			graphics.fillRoundRect(0, 0, getPreferredWidth(),
					getPreferredHeight(), 1, 1);
		}

		super.paint(graphics);
	}

	public boolean isFocusable() {
		return true;
	}

	public int getPreferredWidth() {
		return Display.getWidth();
	}

	protected boolean navigationClick(int status, int time) {
		fieldChangeNotify(0);
		showView();
		return super.navigationClick(status, time);
	}

	protected boolean keyChar(char character, int status, int time) {
		
		if (character == Keypad.KEY_ENTER) {
			fieldChangeNotify(0);
			showView();
			return true;
		}

		/*
		 * else if (character == Keypad.KEY_ESCAPE){ Dialog.alert("Salir"); }
		 */
		return super.keyChar(character, status, time);
	}

	protected boolean touchEvent(TouchEvent message) {

		if (message.getEvent() == TouchEvent.CLICK) {
			fieldChangeNotify(0);
			showView();
			return true;
		}

		return super.touchEvent(message);
	}

	protected void onFocus(int direction) {
		super.onFocus(direction);
		invalidate();
	}

	protected void onUnfocus() {
		super.onUnfocus();
		invalidate();
	}
	
	private void showView() {

		final int REGISTRO = 0;
		final int CONTRASENIA = 1;
		int DEFAULT = REGISTRO;
		int answer = Dialog.ask("Opciones", new String[] {
				"Modificar/Actualizar", "Cambiar contraseņa" }, new int[] {
				REGISTRO, CONTRASENIA }, DEFAULT);

		switch (answer) {
		case REGISTRO:
			//UiApplication.getUiApplication().pushScreen(
					//new WModificarDatos(true, "Modificar Datos"));
			break;
		case CONTRASENIA:
			//UiApplication.getUiApplication().pushScreen(
					//new WModificarContrasenia(true, "Cambiar contraseņa"));
			break;
		}

	}
}
