package com.addcel.prosa.view.base.uicomponents;

import com.addcel.prosa.view.base.UtilColor;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;

public class CustomSelectedButtonField extends Field {
	
	private String label;

	private boolean isSelected = false;

	public CustomSelectedButtonField(String label, long style) {
		super(style);
		this.label = label;
	}
	
	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
		invalidate();
	}

	public int getPreferredHeight() {
		return getFont().getHeight() + 8;
	}

	public int getPreferredWidth() {
		return Display.getWidth()/2;
	}

	protected void layout(int width, int height) {
		setExtent(Math.min(width, getPreferredWidth()), Math.min(height, getPreferredHeight()));
	}

	protected void paint(Graphics graphics) {

		
		if (isFocus()){
			graphics.setColor(UtilColor.BUTTON_FOCUS);
			graphics.fillRoundRect(1, 1, getWidth()-2, getHeight()-2, 12, 12);
			graphics.setColor(UtilColor.BUTTON_STRING_FOCUS);
			//graphics.drawText(label, 4, 4);
			graphics.drawText(label, 0, 4, DrawStyle.HCENTER, this.getWidth());
		} else if (isSelected) {
			graphics.setColor(UtilColor.BUTTON_SELECTED);
			graphics.fillRoundRect(1, 1, getWidth()-2, getHeight()-2, 12, 12);
			graphics.setColor(UtilColor.BUTTON_STRING_SELECTED);
			//graphics.drawText(label, 4, 4);
			graphics.drawText(label, 0, 4, DrawStyle.HCENTER, this.getWidth());
		} else {
			graphics.setColor(UtilColor.BUTTON_UNSELECTED);
			graphics.fillRoundRect(1, 1, getWidth()-2, getHeight()-2, 12, 12);
			graphics.setColor(UtilColor.BUTTON_STRING_UNSELECTED);
			//graphics.drawText(label, 4, 4);
			graphics.drawText(label, 0, 4, DrawStyle.HCENTER, this.getWidth());
		}
	}

	public boolean isFocusable() {
		return true;
	}

	
	public boolean isSelectable() {
		return true;
	}


	protected void drawFocus(Graphics graphics, boolean on) {
	}
	
	protected void onFocus(int direction) {
		super.onFocus(direction);
		invalidate();
	}
	
	protected void onUnfocus() {
		super.onUnfocus();
		invalidate();
	}
	
	protected boolean navigationClick(int status, int time) {
		fieldChangeNotify(0);
		isSelected = !isSelected;
		return true;
	}
	
	protected boolean keyChar(char character, int status, int time) {
		if (character == Keypad.KEY_ENTER) {
			fieldChangeNotify(0);
			isSelected = !isSelected;
			return true;
		}
		return super.keyChar(character, status, time);
	}
}
