package com.addcel.prosa.view.base;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.component.BitmapField;

public class UtilIcon {

	
	public static BitmapField getTitle(){
		
		String resolution = "w" + String.valueOf(Display.getWidth())  + ".png";
		
		Bitmap encodedImage = Bitmap.getBitmapResource(resolution);
		BitmapField bitmapField = new BitmapField(encodedImage);

		return bitmapField;
	}

	
	
	public static BitmapField getTitleBoleto(){
		
		int width = Display.getWidth();

		String name = String.valueOf(width)  + "boletoa.png";
		
		Bitmap encodedImage = Bitmap.getBitmapResource(name);
		
		BitmapField bitmapField = new BitmapField(encodedImage);
		
		return bitmapField;
	}
	
	
	public static BitmapField getBodyBoleto(){
		
		int width = Display.getWidth();

		String name = String.valueOf(width)  + "boleto.png";
		
		Bitmap encodedImage = Bitmap.getBitmapResource(name);
		
		BitmapField bitmapField = new BitmapField(encodedImage);
		
		return bitmapField;
	}
	
	
	public static Bitmap getBodyBoletoBitmap(){
		
		int width = Display.getWidth();

		String name = String.valueOf(width)  + "boleto.png";
		
		Bitmap encodedImage = Bitmap.getBitmapResource(name);
		
		return encodedImage;
	}


	public static BitmapField getSplash(){
		
		int width = Display.getWidth();
		int height = Display.getHeight();
		
		StringBuffer stringBuffer = new StringBuffer();
		
		stringBuffer.append(width).append("_");
		stringBuffer.append(height).append("_");
		stringBuffer.append("splash.png");
		
		String splash = stringBuffer.toString();
		
		Bitmap encodedImage = Bitmap.getBitmapResource(splash);
		BitmapField bitmapField = new BitmapField(encodedImage);
		
		return bitmapField;
	}
	
}
