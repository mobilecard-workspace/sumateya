package com.addcel.prosa.view.base;


import com.addcel.prosa.view.base.uicomponents.SubtitleRichTextField;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.FontFamily;
import net.rim.device.api.ui.Screen;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

import com.addcel.prosa.view.base.uicomponents.ElementLabelField;

public abstract class Viewable extends MainScreen{

	public final static int ROUTE = 1;
	public final static int DEPART = 2;
	public final static int ARRIVE = 3;
	public final static int SCHEDULED = 4;
	public final static int VROUTE = 5;
	public final static int CREATE = 6;

	public final static int ID_SECUENCIA = 7;

	public final static int SEE_DIAGRAM = 10;
	public final static int SEE_OCCUPANCY = 11;

	protected Font font = null;
	private int width;
	private int height;

	public Viewable(boolean isSetTitle, String title){
		
		super(Screen.DEFAULT_CLOSE);
		setBackground(UtilColor.MAIN_BACKGROUND);
		width = Display.getWidth();
		height = Display.getHeight();

		ElementLabelField labelField = new ElementLabelField("Haz tu donativo");
		
		if (isSetTitle){

			add(labelField);
			BitmapField bitmapField = UtilIcon.getTitle();
			add(bitmapField);
			//add(new LabelField(""));
			//SubtitleRichTextField orangeRichTextField01 = new SubtitleRichTextField(title);
			//add(orangeRichTextField01);
			add(new LabelField(""));
		}
	}
	
	public int getPreferredWidth(){
		return width;
	}

	public boolean onSavePrompt(){
	    return true;
	} 
	
	
	public void setBackground(int color){
		
    	VerticalFieldManager manager = null;
    	Background background = null;
    	
    	manager = (VerticalFieldManager)getMainManager();
		background = BackgroundFactory.createSolidBackground(color);
		manager.setBackground(background);
	}

	
	public Font setFont(){
		
		Font font = null;  
		FontFamily alphaSansFamily;
		try {
			alphaSansFamily = FontFamily.forName("BBAlpha Sans Condensed");
		   	font = alphaSansFamily.getFont(Font.PLAIN, 6, Ui.UNITS_pt);
			setFont(font);
		} catch (ClassNotFoundException e) {
			Dialog.alert("No se pudo cargar la Fuente");
		}
	   	
	   	return font;
	}	
	
	
	public void setData(final int request, final Object object) {
				
		UiApplication.getUiApplication().invokeLater(new Runnable() {
			public void run() {
				analyzeData(request, object);
			}
		});
	}
	
	protected abstract void analyzeData(int request, Object object);
}
