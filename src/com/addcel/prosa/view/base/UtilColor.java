package com.addcel.prosa.view.base;

import net.rim.device.api.ui.Color;

public class UtilColor {
	
	private final static int GREEN_01 = 0x90c1c7;
	private final static int GREEN_02 = 0x3db7b0;

	private final static int BLUE = 0x23486d;

	private final static int WHITE = Color.WHITE;
	private final static int RED = 0xc30042;
	
	private final static int GREY_01 = 0xececec;
	private final static int GREY_02 = 0xdfdfdf;
	private final static int GREY_03 = 0xb4bdb6;
	private final static int GREY_04 = 0x939598;
	private final static int GREY_05 = 0x231f20;
	
	
	public final static int MAIN_BACKGROUND = WHITE;

	public final static int TITLE_STRING = WHITE;
	public final static int TITLE_BACKGROUND = GREEN_02;

	public final static int BUTTON_FOCUS = RED;
	public final static int BUTTON_SELECTED = GREY_04;
	public final static int BUTTON_UNSELECTED = GREY_04;

	public final static int BUTTON_STRING_FOCUS = GREY_01;
	public final static int BUTTON_STRING_SELECTED = WHITE;
	public final static int BUTTON_STRING_UNSELECTED = WHITE;

	public final static int SUBTITLE_STRING = GREY_01;
	public final static int SUBTITLE_BACKGROUND = GREEN_02;

	public final static int ELEMENT_STRING = RED;
	public final static int ELEMENT_BACKGROUND = GREY_01;

	public final static int EDIT_TEXT_DATA_FOCUS = GREY_05;
	public final static int EDIT_TEXT_DATA_UNFOCUS = GREY_04;
	
	public final static int EDIT_TEXT_BACKGROUND_FOCUS = GREY_01;
	public final static int EDIT_TEXT_BACKGROUND_UNFOCUS = GREY_02;

	public final static int LIST_DESCRIPTION_TITLE = GREEN_02;
	public final static int LIST_DESCRIPTION_DATA = GREY_05;

	public final static int LIST_BACKGROUND_SELECTED = GREEN_02;
	public final static int LIST_BACKGROUND_UNSELECTED = GREY_01;


	public static int toRGB(int r, int g, int b){
		return (r<<16)+(g<<8)+b;		 
	}

	public static int getBlack(){
		return toRGB(30, 30, 30);
	}

	public static int getDarkGray(){
		return toRGB(120, 120, 120);
	}

	public static int getLightGray(){
		return toRGB(200, 200, 200);
	}

	public static int getLightOrange(){
		return toRGB(255, 184, 101);
	}

	public static int getDarkOrange(){
		return toRGB(255, 159, 45);
	}
}
