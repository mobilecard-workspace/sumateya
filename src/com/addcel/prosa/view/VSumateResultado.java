package com.addcel.prosa.view;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.LabelField;

import com.addcel.prosa.dto.PagoRespuesta;
import com.addcel.prosa.view.base.Viewable;
import com.addcel.prosa.view.base.uicomponents.CustomSelectedSizeButton;
import com.addcel.prosa.view.base.uicomponents.ElementLabelField;

public class VSumateResultado extends Viewable implements FieldChangeListener{
	
	private ElementLabelField mensajeErrorTxt;
	private ElementLabelField referenciaTxt;
	private ElementLabelField autorizacionTxt;	

	private ElementLabelField mensajeErrorEdit;
	private ElementLabelField referenciaEdit;
	private ElementLabelField autorizacionEdit;
	
	private CustomSelectedSizeButton salir;
	
	PagoRespuesta pagoRespuesta;
	
	public VSumateResultado(boolean isSetTitle, String title,
			PagoRespuesta pagoRespuesta) {
		super(isSetTitle, title);

		this.pagoRespuesta = pagoRespuesta;

		mensajeErrorTxt = new ElementLabelField("Resultado");
		referenciaTxt = new ElementLabelField("Referencia");
		autorizacionTxt = new ElementLabelField("Autorización");

		mensajeErrorEdit = new ElementLabelField(
				pagoRespuesta.getMensajeError());
		referenciaEdit = new ElementLabelField(String.valueOf(pagoRespuesta
				.getReferencia()));
		autorizacionEdit = new ElementLabelField(String.valueOf(pagoRespuesta
				.getAutorizacion()));

		add(mensajeErrorTxt);
		add(mensajeErrorEdit);
		add(new LabelField());
		add(referenciaTxt);
		add(referenciaEdit);
		add(new LabelField());
		add(autorizacionTxt);
		add(autorizacionEdit);

		if (pagoRespuesta.getIdError() == 0) {
			salir = new CustomSelectedSizeButton("Aceptar", 1);

		} else {
			salir = new CustomSelectedSizeButton("Intentar de nuevo", 1);
		}

		salir.setChangeListener(this);
		add(salir);
	}

	public void fieldChanged(Field field, int context) {
		
		if (field == salir){
			
			if (pagoRespuesta.getIdError() == 0){
				System.exit(0);
			} else {
				UiApplication.getUiApplication().popScreen(this);
			}
		}
	}

	protected void analyzeData(int request, Object object) {
	}
}
