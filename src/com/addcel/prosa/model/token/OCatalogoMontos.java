package com.addcel.prosa.model.token;

import java.util.Vector;

import org.json.me.JSONArray;
import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.api.addcelexception.Error;
import com.addcel.api.addcelexception.OwnException;
import com.addcel.api.dataaccess.components.toObject.Objectable;
import com.addcel.prosa.dto.Monto;

public class OCatalogoMontos implements Objectable{

	
	private Vector catalogoMontos;
	
	public void execute(String data) throws OwnException {
		
		try {
//			JSONObject jsonObject = new JSONObject(data);
			
			JSONArray jsonArray = new JSONArray(data); 
			
			int length = jsonArray.length();
			
			catalogoMontos = new Vector();
			
			for(int index = 0; index < length; index++){
				
				JSONObject jsonObject = jsonArray.optJSONObject(index);
				
				Monto monto = new Monto();
				
				monto.setIdMonto(jsonObject.optInt("idMonto"));
				monto.setMonto(jsonObject.optInt("monto"));
				
				catalogoMontos.addElement(monto);				
			}
		} catch (JSONException e) {
			e.printStackTrace();
			throw new OwnException(Error.JSON_EXCEPTION);
		}
	}

	public Object getData() {
		return catalogoMontos;
	}

}
