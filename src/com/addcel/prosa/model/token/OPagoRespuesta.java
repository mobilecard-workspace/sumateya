package com.addcel.prosa.model.token;

import org.json.me.JSONException;
import org.json.me.JSONObject;

import com.addcel.api.addcelexception.Error;
import com.addcel.api.addcelexception.OwnException;
import com.addcel.api.dataaccess.components.toObject.Objectable;

import com.addcel.prosa.dto.PagoRespuesta;

public class OPagoRespuesta implements Objectable{

	private PagoRespuesta pagoRespuesta;
	
	public void execute(String data) throws OwnException {

		pagoRespuesta = new PagoRespuesta(); 
		
		try {
			JSONObject jsonObject = new JSONObject(data);
			
			pagoRespuesta.setIdError(jsonObject.optInt("idError"));
			pagoRespuesta.setMensajeError(jsonObject.optString("mensajeError"));
			pagoRespuesta.setReferencia(jsonObject.optInt("referencia"));
			pagoRespuesta.setAutorizacion(jsonObject.optInt("autorizacion"));
			
		} catch (JSONException e) {
			e.printStackTrace();
			throw new OwnException(Error.JSON_EXCEPTION);
		}
	}

	public Object getData() {
		return pagoRespuesta;
	}

}
