package com.addcel.prosa.model.token;

import java.util.Date;

import com.addcel.api.dataaccess.components.encryption.Encryptionable;
import com.addcel.api.util.security.AddcelCrypto;

public class EncryptSensitive implements Encryptionable{

	private String data;
	
	public void execute(String data) {

		Date hoy = new Date();
		long lkey = hoy.getTime();
		String skey = String.valueOf(lkey);
		
		int length = skey.length();
		
		skey = skey.substring(length - 8, length);
		
		this.data = AddcelCrypto.encryptSensitive(skey, data);
	}

	public Object getData() {
		return data;
	}

}
