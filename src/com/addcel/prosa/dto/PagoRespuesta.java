package com.addcel.prosa.dto;

/*
{
	"idError":0,
	"mensajeError":"El pago fue exitoso.",
	"referencia":101,
	"autorizacion":16094
}
*/


public class PagoRespuesta {

	private int idError;
	private String mensajeError;
	private int referencia;
	private int autorizacion;
	
	public int getIdError() {
		return idError;
	}
	public void setIdError(int idError) {
		this.idError = idError;
	}
	public String getMensajeError() {
		return mensajeError;
	}
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	public int getReferencia() {
		return referencia;
	}
	public void setReferencia(int referencia) {
		this.referencia = referencia;
	}
	public int getAutorizacion() {
		return autorizacion;
	}
	public void setAutorizacion(int autorizacion) {
		this.autorizacion = autorizacion;
	}
	
	

}
