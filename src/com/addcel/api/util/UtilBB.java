package com.addcel.api.util;

import net.rim.device.api.servicebook.ServiceBook;
import net.rim.device.api.servicebook.ServiceRecord;
import net.rim.device.api.system.CoverageInfo;
import net.rim.device.api.system.DeviceInfo;
import net.rim.device.api.system.GPRSInfo;
import net.rim.device.api.system.WLANInfo;

public class UtilBB {

	public static final int DEVICE_NAME = 0;
	public static final int SOFTWARE_VERSION = 1;
	public static final int MANUFACTURER_NAME = 2;

	public static String getImei() {
		byte[] imei;
		
		String value = null;
		
		try {
			imei = GPRSInfo.getIMEI();

			value = GPRSInfo.imeiToString(imei, false);
		} catch (Exception e) {
			e.printStackTrace();

			int id = DeviceInfo.getDeviceId();
			value = Integer.toString(id);
			
		}
		return value;
	}

	public static String getDeviceINFO(int key) {

		String value = null;

		switch (key) {
		case DEVICE_NAME:
			value = DeviceInfo.getDeviceName();
			break;

		case SOFTWARE_VERSION:
			value = DeviceInfo.getSoftwareVersion();
			break;

		case MANUFACTURER_NAME:
			value = DeviceInfo.getManufacturerName();
			break;
		}

		return value;
	}

	public static ServiceRecord getWAP2ServiceRecord() {
		ServiceBook sb = ServiceBook.getSB();
		ServiceRecord[] records = sb.getRecords();

		for (int i = 0; i < records.length; i++) {
			String cid = records[i].getCid().toLowerCase();
			String uid = records[i].getUid().toLowerCase();
			if (cid.indexOf("wptcp") != -1 && uid.indexOf("wifi") == -1
					&& uid.indexOf("mms") == -1) {
				return records[i];
			}
		}

		return null;
	}

	public static String checkConnectionType() {
		String idealConnection = null;

		if (WLANInfo.getWLANState() == WLANInfo.WLAN_STATE_CONNECTED) {
			// Connected to a WiFi access point
			idealConnection = "interface=wifi";
		} else {
			int coverageStatus = CoverageInfo.getCoverageStatus();
			ServiceRecord record = getWAP2ServiceRecord();
			if (record != null
					&& (coverageStatus & CoverageInfo.COVERAGE_DIRECT) == CoverageInfo.COVERAGE_DIRECT) {

				idealConnection = "deviceside=true;ConnectionUID="
						+ record.getUid();
			} else if ((coverageStatus & CoverageInfo.COVERAGE_MDS) == CoverageInfo.COVERAGE_MDS) {
				// Have an MDS service book and network coverage
				idealConnection = "deviceside=false";
			} else if ((coverageStatus & CoverageInfo.COVERAGE_DIRECT) == CoverageInfo.COVERAGE_DIRECT) {
				// Have network coverage but no WAP 2.0 service book record
				idealConnection = "deviceside=true";
			}
		}

		return idealConnection;
	}
}
