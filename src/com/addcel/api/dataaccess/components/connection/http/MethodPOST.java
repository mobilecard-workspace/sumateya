package com.addcel.api.dataaccess.components.connection.http;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;

import com.addcel.api.addcelexception.OwnException;
import com.addcel.api.dataaccess.components.connection.Connectable;


import com.addcel.api.addcelexception.Error;
import com.addcel.prosa.Start;

import net.rim.device.api.io.http.HttpProtocolConstants;


/*
	Some other notes on POST requests:
	
	POST requests are never cached
	POST requests do not remain in the browser history
	POST requests cannot be bookmarked
	POST requests have no restrictions on data length
*/


public class MethodPOST implements Connectable {

	private HttpConnection httpConnection;
	private InputStream inputStream;
	
	private String data = null;
	private String url = null;
	
	boolean error = false;
	StringBuffer stringBuffer = null;
	
	public MethodPOST(String url){
		this.url = url;
	}
	

	public String createURL() {
		
		//return url = url + Start.IDEAL_CONNECTION;
		
		String newURL = url + ";" + Start.IDEAL_CONNECTION + ";" + "ConnectionTimeout=30000";
		
		return newURL;
	}
	
	private void sendData() throws OwnException{
		
		try{
			
			stringBuffer = new StringBuffer();

			httpConnection = (HttpConnection) Connector.open(createURL());

			httpConnection.setRequestMethod(HttpConnection.POST);

			byte[] postBytes = data.getBytes();
			
			httpConnection.setRequestProperty(
					HttpProtocolConstants.HEADER_CONTENT_LENGTH,
					Integer.toString(postBytes.length));
			
			OutputStream strmOut = httpConnection.openOutputStream();
			strmOut.write(postBytes);
			strmOut.flush();
			strmOut.close();
		} catch(IOException oie){
			error = true;
			throw new OwnException(Error.HTTP_SEND_DATA);
		}
	}
	
	
	private void getInfo() throws OwnException{
		
		try {
			
			inputStream = httpConnection.openInputStream();
			int c;
			while ((c = inputStream.read()) != -1) {

				stringBuffer.append((char) c);
			}

			System.out.println(stringBuffer.toString());

		}  catch(IOException oie){
			error = true;
			throw new OwnException(Error.HTTP_RECEIVE_DATA);
		} 
	}
	
	
	public void execute(String data) throws OwnException {

		try{

			this.data = data;
			
			sendData();			
			
			if (!error){
				
				getInfo();
			}


		} finally {

			try {
				if (inputStream != null) {
					inputStream.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				if (httpConnection != null) {
					httpConnection.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public Object getData() {
		return stringBuffer.toString();
	}
}
